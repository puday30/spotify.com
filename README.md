This Project is Automated test cases on Spotify API.
Framework used is TestNg Framework for RestAssured.
Allure reporting is used.
All the important variables are stored in the properties file.
CRUD calls are tested for Spotify.
Authentication is approved from the server side in the form of access code.Register the app and note down the Client_id and Client_secret.
 Steps to drive the access code:
 1.Run the API in the browser:
 https://accounts.spotify.com/authorize?client_id=9ac0c334f84d4627be0f48e9755c8c69&response_type=code&redirect_uri=https://localhost:8888/callback&scope=playlist-read-private playlist-read-collaborative playlist-modify-private playlist-modify-public&state=34fFs29kd09
 2.Truncate the Code.
 3.Pass the code in the API https://accounts.spotify.com/api/token as code parameter.
 4.Get the "access Code " from response.
