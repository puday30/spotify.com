package com.qa.spotify;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.example.CreatePlayListPayload;
import com.qa.API.PlayListAPI;

import io.restassured.specification.RequestSpecification;

public class UserProfileTestCases  {


	String UserID;

	public String playlistID;

	@AfterMethod
	public void afterMethod(ITestResult result) {
		System.out.println("Executed method name:" + result.getMethod().getMethodName());
	}

	@Story("GET USERID API")
	@Description("Validation to GET userID API")
	@Test(priority=1)
	public void UserProfileTestcase001() throws IOException {


		Response response=PlayListAPI.API_GetUserID();

		UserID=PlayListAPI.API_GetUserID().path("id");
		Allure.step("Assertting User ID Status Code");
		Assert.assertEquals(response.getStatusCode(), 200);

	}

	@Story("Create PlayLIST API")
	@Description("Validation to Create PlayList API")
	@Test(priority=2,enabled=false)
	public void PlayListTestCase001() throws IOException {

		Allure.step("Get the User ID from UserProfileTestcase001" );
		System.out.println("User id received from UserProfileTestcase001 is --->"+UserID);
		Response response= PlayListAPI.API_Post_CreatePlayList(UserID);

		playlistID=response.path("id");
		Allure.step("Asserting the Status Code" );
		Assert.assertEquals(response.getStatusCode(), 201);

	}

	@Story("Get PlayLIST API")
	@Description("Validation to Get PlayList API")
	@Test(priority=3,enabled=false)
	public void PlayListGetTestCase002() throws IOException {

		System.out.println("Playlist ID received from PlayListTestCase001 --->"+playlistID);
		Allure.step("Catptured playlist ID from PlayListTestCase001 in playlistID variable" );

		Response response=PlayListAPI.API_Get_Playlist(playlistID);
		//JsonPath jsonpath=new JsonPath(response.asString());

		SoftAssert softassert=new SoftAssert();
		Allure.step("Asserting the Status Code" );
		Assert.assertEquals(response.getStatusCode(), 200);
		Allure.step("Asserting the playlist ID with the expected value" );
		Assert.assertEquals(response.path("id"), playlistID);

		softassert.assertAll();
	}
	@Story("Put PlayLIST API")
	@Description("Validation to update PlayList API")
	@Test(priority=4,enabled=false)
	public void PlayListGetTestCase003() throws IOException {

		System.out.println("Playlist ID received from PlayListTestCase001 --->"+playlistID);
		Allure.step("Catptured playlist ID from PlayListTestCase001 in playlistID variable" );
		Response response=PlayListAPI.API_Put_Playlist(playlistID);
		Allure.step("Asserting the Status Code" );
		Assert.assertEquals(response.getStatusCode(), 200);


	}

}
