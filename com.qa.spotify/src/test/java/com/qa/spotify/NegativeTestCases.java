package com.qa.spotify;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import io.qameta.allure.Owner;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.example.CreatePlayListPayload;
import com.example.Example;
import java.nio.file.Files;
import com.qa.API.PlayListAPI;
import com.qa.API.TokenRequest;

@Epic("Neative Sceanrios to verify the Error messages")
@Feature("error Messages")
@Owner("Priyanka")
public class NegativeTestCases extends TestBase {

	public NegativeTestCases() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	Properties prop;
	@BeforeMethod
	public void setUp() throws IOException {
		
		 prop=PlayListAPI.initialiszepropperties();
	}
	@AfterMethod
	public void afterMethod(ITestResult result) {
	  System.out.println("Executed method name:" + result.getMethod().getMethodName());
	}
	
	@Step
	@Story("Access token validation")
	@Issue("D543456")
	@Description ("Validate the error message when Access token given in request has expired ")
	@Test(description="Validate the Access token has expired")
	public void NegativeTestCase001() throws IOException {
	
		
		Allure.step("Adding Request Playlist");
		CreatePlayListPayload payload=new CreatePlayListPayload("new Playlist","New playlist description",false);
	String access_token="Bearer "+"AQDRCsJMeE-L8T9YGqT-06oMro7kiEfu1i6fgBurQv5hQgdL8h5UqYlFQmgwYl_wnK8DwykLYt13xpFN4h_V8wYfVpZb-Y-h578qVH1WLtFN8vPy29Hda3lPVs9I69jxkAmvHzanhwYfXVq0fowOgfX64vLp46a64Lq40J0ic_kd_MSFLH-8TrOYq5vy6Rc5Qa9pzph7WbbQDzU2jx6rXpdQkNcZQFUjvbMrSY0o3xC2Vqai0-o-FGCmMIn67ZVhuP-arYPCEY8se9tNosYRtbEKmtNc_mfGMNMlwmLzmpnGeeRB5IDp_g";
		
		Map<String,Object> headerMap = new HashMap<String,Object>();
		headerMap.put("Content-Type","application/json");
		headerMap.put("Authorization",access_token);
		System.out.println("base uRI is"+prop.getProperty("URI")+prop.getProperty("ServiceURI_negativeTescase1"));
		
		Allure.step("Sending request for create playlist");
		given().baseUri(prop.getProperty("URI")).headers(headerMap).body(payload).log().all().filter(new AllureRestAssured()).
		when().post(prop.getProperty("ServiceURI_negativeTescase1")).
		then().log().all().assertThat().statusCode(401).
		body("error.status", equalTo(401));
		Allure.step("Validating response");

	      
	
	}
	@Step
	@Story("Access token validation")
	@Description("Validate the error message when Access token given in request is not valid")
	@Test(description="Validate the access token is Invalid")
	public void NegativeTestCase002() throws IOException {
		
		CreatePlayListPayload payload=new CreatePlayListPayload("new Playlist","New playlist description",false);
		
		Map<String,Object> headerMap = PlayListAPI.requestHeader();
		
		Example error=given().baseUri(prop.getProperty("URI")).headers(headerMap).body(payload).log().all().filter(new AllureRestAssured()).
		when().post(prop.getProperty("ServiceURI_negativeTescase1")).
		then().log().all().extract().as(Example.class);
		
		
		if(error.getError().getMessage().contains("Invalid"))
			Assert.assertEquals(error.getError().getMessage(),"Invalid access token");
		else
		Assert.assertEquals(error.getError().getMessage(),"The access token expired");
		
		Assert.assertEquals(error.getError().getStatus(),401);
	
		
	}
	@Step
	@Story("Name field is not provided")
	@Description("Validate the Error message when Name is not provided in the Request of Create Playlist API")
	@Test(description="Validate the error message when Name is not provided in the Create Playlist API")
	public void NegativeTestCase003() throws IOException {
		
		CreatePlayListPayload payload=new CreatePlayListPayload("","New playlist description",false);
		Map<String,Object> headerMap = PlayListAPI.requestHeader();
		
		
		Example error=given().baseUri(prop.getProperty("URI")).headers(headerMap).body(payload).log().all().filter(new AllureRestAssured()).
		when().post(prop.getProperty("ServiceURI_negativeTescase1")).
		then().log().all().extract().as(Example.class);
		
		
		Assert.assertEquals(error.getError().getMessage(),"Missing required field: name");
		Assert.assertEquals(error.getError().getStatus(),400);
		
	}
	@Step
	@Test(enabled=false)
	public void GetTokenTestcase001() throws InterruptedException, IOException {
		
		String token=TokenRequest.getToken();
		
		System.out.println(token);
	}

	
}
