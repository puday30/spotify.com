package com.qa.API;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import com.qa.API.TokenRequest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.decorators.WebDriverDecorator;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import com.example.CreatePlayListPayload;

import java.io.FileInputStream;

public class PlayListAPI  {
	
	
	
public static Properties initialiszepropperties() throws IOException {
	
	String path="C:\\Users\\ravin\\eclipse-workspace\\com.qa.spotify\\src\\test\\java\\com\\qa\\API\\testdata.properties";
	Properties prop=new Properties();
	File f=new File(path);
	FileInputStream fp=new FileInputStream(f);
	

	prop.load(fp);
	
	return prop;
}


	static String URI;
	
	
	static String access_token;

	public static Response  API_GetUserID( ) throws IOException {
		Properties prop=initialiszepropperties();
		URI=prop.getProperty("URI");

		access_token="Bearer "+TokenRequest.getToken();
		String ServiceURI=prop.getProperty("ServiceURI_GetUserID");
		System.out.println(access_token);

		Response response=given().baseUri(URI).header("Authorization",access_token).log().all().get(ServiceURI);


		return response;
	}

	
	public static Map<String,Object> requestHeader() throws IOException {
		access_token="Bearer "+TokenRequest.getToken();
		Map<String,Object> headerMap = new HashMap<String,Object>();
		headerMap.put("Content-Type","application/json");
		headerMap.put("Authorization",access_token);
		
		System.out.println(headerMap);
		return headerMap;
	}
	
	public static Response API_Post_CreatePlayList(String UserID) throws IOException {
		Properties prop=initialiszepropperties();
		URI=prop.getProperty("URI");
		access_token="Bearer "+TokenRequest.getToken();
		String ServiceURI=prop.getProperty("ServiceURI1_CreatePlaylist")+UserID+prop.getProperty("ServiceURI2_CreatePlaylist");
		Map<String,Object> headerMap = new HashMap<String,Object>();
		headerMap.put("Content-Type","application/json");
		headerMap.put("Authorization",access_token);


		CreatePlayListPayload payload=new CreatePlayListPayload("New Playlist","New playlist description",false);
		Response response= given().baseUri(URI).headers(headerMap).body(payload).log().all().post(ServiceURI);

		return response;
	}


	public static Response API_Get_Playlist(String playlistID) throws IOException {

		Properties prop=initialiszepropperties();
		URI=prop.getProperty("URI");
	
		
		access_token="Bearer "+TokenRequest.getToken();
		String ServiceURI=prop.getProperty("ServiceURI_GetPlaylistID")+playlistID;

		Map<String,Object> headerMap = new HashMap<String,Object>();
		headerMap.put("Authorization",access_token);

		Response response=given().baseUri(URI).headers(headerMap).log().all().when().get(ServiceURI);
		return response;
	}


	public static Response API_Put_Playlist(String playlistID) throws IOException {

		Properties prop=initialiszepropperties();
		URI=prop.getProperty("URI");
	
		
		access_token="Bearer "+TokenRequest.getToken();
		String ServiceURI=prop.getProperty("ServiceURI_PutPlaylistID")+playlistID;
		Map<String,Object> headerMap = new HashMap<String,Object>();
		headerMap.put("Content-Type","application/json");
		headerMap.put("Authorization",access_token);

		CreatePlayListPayload payload=new CreatePlayListPayload("New Playlist","New playlist description",false);
		Response response=given().baseUri(URI).headers(headerMap).body(payload).log().all().when().put(ServiceURI);
		return response;
	}
	

	
	
	public static String launchdriver() throws InterruptedException {
		
		System.setProperty(
	            "webdriver.chrome.driver",
	            "C:\\Users\\ravin\\eclipse-workspace\\com.qa.spotify\\src\\test\\resources\\chromedriver_win32\\chromedriver.exe");
	String URL="https://accounts.spotify.com/authorize?client_id=9ac0c334f84d4627be0f48e9755c8c69&response_type=code&redirect_uri=https://localhost:8888/callback&scope=playlist-read-private playlist-read-collaborative playlist-modify-private playlist-modify-public&state=34fFs29kd09";
		
		 WebDriver driver=new ChromeDriver();
		 driver.get(URL);	 
		Thread.sleep(5);
		 String currentURL=driver.getCurrentUrl();
		 
		 String[]array=currentURL.split("=");
		 String[]array2= array[0].split("&");
	array2[0]=array2[0].trim();
	
	System.out.println(array2[0]);
	return array2[0];
	}
	
}
