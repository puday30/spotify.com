package com.qa.API;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.restassured.response.Response;

public class TokenRequest  {

	
	public static String getToken() throws IOException {
		
		Map<String,String>formdata=new HashMap<String,String>();
		
		Properties prop=PlayListAPI.initialiszepropperties();
		String access_token;
		
		formdata.put("client_id", prop.getProperty("client_id"));
		formdata.put("client_secret", prop.getProperty("client_secret"));
		formdata.put("grant_type", prop.getProperty("grant_type"));
		formdata.put("code", prop.getProperty("code"));
		formdata.put("redirect_uri", prop.getProperty("redirect_uri"));
		
		String URI=prop.getProperty("AccesstokenURI");
		String ServiceURI=prop.getProperty("AccessTokenserviceURI");
		
		Response response=given().baseUri(URI).
		contentType("application/x-www-form-urlencoded").
		formParams(formdata).when().
		post(ServiceURI).then().log().all().
		extract().response();
		
		if(response.statusCode()==200) {
			
			access_token=	response.path("access_token");
		}
		else {
			access_token="AQDRCsJMeE-L8T9YGqT-06oMro7kiEfu1i6fgBurQv5hQgdL8h5UqYlFQmgwYl_wnK8DwykLYt13xpFN4h_V8wYfVpZb-Y-h578qVH1WLtFN8vPy29Hda3lPVs9I69jxkAmvHzanhwYfXVq0fowOgfX64vLp46a64Lq40J0ic_kd_MSFLH-8TrOYq5vy6Rc5Qa9pzph7WbbQDzU2jx6rXpdQkNcZQFUjvbMrSY0o3xC2Vqai0-o-FGCmMIn67ZVhuP-arYPCEY8se9tNosYRtbEKmtNc_mfGMNMlwmLzmpnGeeRB5IDp_g";
		
		}
		return access_token;
		
	}
	
}
