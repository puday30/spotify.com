package com.example;



import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"error"
})

public class Example {

@JsonProperty("error")
private Error error;

/**
* No args constructor for use in serialization
*
*/
public Example() {
}

/**
*
* @param error
*/
public Example(Error error) {
super();
this.error = error;
}

@JsonProperty("error")
public Error getError() {
return error;
}

@JsonProperty("error")
public void setError(Error error) {
this.error = error;
}

}
