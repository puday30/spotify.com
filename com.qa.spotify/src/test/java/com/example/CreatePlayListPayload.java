package com.example;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePlayListPayload {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	



	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("public")
	private Boolean _public;
	public CreatePlayListPayload(String name, String description, Boolean _public) {
		super();
		this.name = name;
		this.description = description;
		this._public = _public;
		}

		@JsonProperty("name")
		public String getName() {
		return name;
		}

		@JsonProperty("name")
		public void setName(String name) {
		this.name = name;
		}

		@JsonProperty("description")
		public String getDescription() {
		return description;
		}

		@JsonProperty("description")
		public void setDescription(String description) {
		this.description = description;
		}

		@JsonProperty("public")
		public Boolean getPublic() {
		return _public;
		}

		@JsonProperty("public")
		public void setPublic(Boolean _public) {
		this._public = _public;
		}

}
